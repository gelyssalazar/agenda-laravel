<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Agenda</title>
	<style>
		.estilo{
			color: skyblue;
			text-align: center;
		}

		.boton{
			border-radius: 8px;
			color:blue;
			background: #D8D8D8;
			height: 35px;
			width: 200px;
			font-size: 18px;
			color: #58ACFA;
			font-family: sans-serif;
			text-decoration: none;
		}
		.boton:hover{
			cursor: pointer;
		}
		.container{
			width: 80%;
			border:black;
			margin: 0 auto;
		}
		.conta{
			width: 50%;
			border:black;
			margin: 0 auto;
		}
	</style>
</head>
<body>
	<div class="container">
		<h1 class='estilo'>Agenda de Prueba</h1>
		@yield('contenido')	
	</div>
	
</body>
</html>