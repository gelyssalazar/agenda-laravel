@extends('layout')

@section('contenido')
	
	<h2>Import Archivo de :</h2>
	<!-- 	//formulario para la carga de excel -->
	<form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 20px;" action="{{route('excel.import')}}" method="post" enctype="multipart/form-data">
		<input type="file" id="import" name="import_file" onChange=""/>
		{{ csrf_field() }}
		<br/>
		<input type="submit" class="boton" style="margin-top: 20px; width: 90px;" value="Import">
	</form>
	<br/>		
	<a style="float: right; margin-top: 2%;" href="{{route('agenda.home')}}"><button class="boton" > Volver</button></a>
@stop