@extends('layout')

@section('contenido')
<h1>Todos los contactos</h1>
	<table width="100%" border="1">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Email</th>
				<th>Telefono</th>
				<th>Direccion</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($message as $msj)
			<tr>
				<td>{{$msj->nombre}}</td>
				<td>{{$msj->correo}}</td>
				<td>{{$msj->telefono}}</td>
				<td>{{$msj->direccion}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	<a style="float: right; margin-top: 2%;" href="{{route('agenda.home')}}"><button class="boton" > Volver</button></a>
@stop