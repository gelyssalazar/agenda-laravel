@extends('layout')
@section('contenido')
	<div class="container">
		<form method="POST" action="{{route('agenda.store')}}">
			{!! csrf_field()!!}
			<p>
				<label for="nombre">
				Nombre
				<input type="text" name="nombre" value="{{old('nombre')}}">
				{!! $errors->first('nombre', '<span class=error>:message</span>')!!}
			</label>
			</p>
			<p>
				<label for="email">
				email
				<input type="text" name="email" value="{{old('email')}}">
				{!!$errors->first('email', '<span class=error>:message</span> ')!!}
			</label>
			</p>
			<p>
			<label for="telefono">
				telefono
				<input type="text" name="telefono" value="{{old('telefono')}}">
				{!!$errors->first('telefono', '<span class=error>:message</span>') !!}
			</label>
			</p>
			<p>
				<label for="direccion">
				direccion
				<input type="text" name="direccion" value="{{old('direccion')}}">
				{!!$errors->first('direccion', '<span class=error>:message</span> ')!!}
			</label>
			</p>
			<p>
				<input type="submit" value="enviar">
			</p>

			
		</form>
	</div>
	
@stop