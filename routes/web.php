<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as'=>'agenda.home', 'uses'=>'AgendaController@home']);
Route::get('agenda/agregar', ['as'=>'agenda.create','uses'=>'AgendaController@create']);
Route::post('agenda', ['as'=>'agenda.store', 'uses'=>'AgendaController@store']);
Route::get('agenda/mostrar', ['as'=>'agenda.index','uses'=>'AgendaController@index']);
Route::get('loadExcel', ['as'=>'excel.load','uses'=>'ExcelController@load']);
Route::post('importExcel', ['as'=>'excel.import','uses'=>'ExcelController@importExcel']); 