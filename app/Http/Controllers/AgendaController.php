<?php

namespace App\Http\Controllers;
use DB;
use App\Agenda;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\CreateContactAgend;

class AgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$message=DB::table('agenda')->get();
        $message= Agenda::all();
        return view('index', compact('message'));
    }
    public function home()
    {
        return view('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('agregar');    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateContactAgend $request)
    {
           /*DB::table('agenda')->insert([
                "nombre"=> $request->input('nombre'),
                "correo"=> $request->input('email'),
                "telefono"=> $request->input('telefono'),
                "direccion"=> $request->input('direccion'),
                //libreria carbon maneja la fecha actual
                "created_at"=> Carbon::now(),
                 "updated_at"=> Carbon::now(),
            ]);*/

            //guardar por el modelo
            $agenda= new Agenda;
            $agenda->nombre=$request->input('nombre');
            $agenda->correo=$request->input('email');
            $agenda->telefono=$request->input('telefono');
            $agenda->direccion=$request->input('direccion');
            //metodo 1 para guardar
            //$agenda->save();
            //metodo 2 para guardar
            Agenda::insert([
                "nombre"=>$request->input('nombre'),
                 "correo"=>$request->input('email'),
                "telefono"=>$request->input('telefono'),
                "direccion"=>$request->input('direccion')
            ]);



        //redireccionar

            return redirect()->route('agenda.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return"vamos a mostrar";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
