<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Agenda;
use Excel;
use App\Http\Requests\importExcelRequest;

class ExcelController extends Controller
{
   	/**
     * Import file into database Code
     *
     * @var array
     */
	
 	public function load()
    {
        return view('import');
    }

	public function importExcel(Request $request){


		if($request->hasFile('import_file')){
			$path = $request->file('import_file')->getRealPath();
			$data = Excel::load($path, function($reader) {})->get();
			if(!empty($data) && $data->count()){
				foreach ($data->toArray() as $key => $value) {
						foreach ($value as $v) {		
							$insert[] = ['nombre' => $v['nombre'], 
							'correo' => $v['correo'],
							 'telefono' => $v['telefono'],
							 'direccion' => $v['direccion']
							 ];
						}
					}
				
				}

			if(!empty($insert)){
				Agenda::insert($insert);
				return ('success Insert Record successfully.');
			}
			
		}
		return'ha habido un error';

	}

}
